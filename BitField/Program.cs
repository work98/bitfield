﻿using System;

namespace BitField
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            //Creating two bitfields with same element's quantity
            var a = new BitField(6);
            var b = new BitField(6);

            //Randomly initilize bitfields
            for (int i = 0; i <a.Size; i++)
            {
                a.setvalue(i, random.Next(0, 2) == 0 ? false : true);
                b.setvalue(i, random.Next(0, 2) == 0 ? false : true);
            }
            Console.WriteLine(a);// [1, 0, 1, 0, 0, 1]
            Console.WriteLine(b);// [1, 0, 1, 0, 1, 1]
            //Some operations demonstration

            //intersection (logical [AND] for each element)
            Console.WriteLine(a * b);// [1, 0, 1, 0, 0, 1]

            //unification (logical [OR] for each element)
            Console.WriteLine(a + b);// [1, 0, 1, 0, 1, 1]

            //inversion (logical [NOT] for each element)
            Console.WriteLine(!a);// [0, 1, 0, 1, 1, 0]
            Console.WriteLine(!b);// [0, 1, 0, 1, 0, 0]
        }

    }
}
