﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BitField
{
    public class IncompatibleSizeException: Exception { }
    public class BitField
    {
        private ulong[] values;
        private uint size;
        public int Size => (int)this.size;
        public int Active => new List<int>((int)size).Sum(p => this.getvalue(p) ? 1 : 0);
        public BitField(uint size)
        {
            values = new ulong[size/sizeof(long)+1];
            this.size = size;
            initZero();
        }

        private void initZero()
        {
            for (int i=0;i<values.Length/sizeof(ulong)+1;i++)
            {
                this.values[i] = 0;
            }
        }

        public void setvalue(int key, bool value)
        {
            if (key > (size - 1)) throw new ArgumentOutOfRangeException();
            if (value)
                this.values[key / sizeof(ulong)] |= (ulong)Math.Pow(2, key % sizeof(ulong));
            else
                this.values[key / sizeof(ulong)] &= ~(ulong)Math.Pow(2, key % sizeof(ulong));
        }

        public bool getvalue(int key)
        {
            if (key > (size - 1)) throw new ArgumentOutOfRangeException();
            ulong val = (char)this.values[key / sizeof(ulong)];
            return (val & (ulong)Math.Pow(2, key % sizeof(ulong)))==0?false:true;
        }

        public override string ToString()
        {
            int[] buff = new int[this.size];
            for(int i=0;i<this.size;i++)
            {
                buff[i] = getvalue(i)?1:0;
            }
            return JsonConvert.SerializeObject(buff);
        }

        public static BitField operator* (BitField a, BitField b)
        {
            if (a.size != b.size) throw new IncompatibleSizeException();
            var field = new BitField(a.size);
            for (int i=0;i<field.size/sizeof(ulong)+1;i++)
            {
                field.values[i] = a.values[i] & b.values[i];
            }
            return field;
        }
        public static BitField operator +(BitField a, BitField b)
        {
            if (a.size != b.size) throw new IncompatibleSizeException();
            var field = new BitField(a.size);
            for (int i = 0; i < field.size / sizeof(ulong) + 1; i++)
            {
                field.values[i] = a.values[i] | b.values[i];
            }
            return field;
        }

        public static BitField operator !(BitField a)
        {
            var field = new BitField(a.size);
            for (int i = 0; i < field.size / sizeof(ulong) + 1; i++)
            {
                field.values[i] = ~a.values[i];
            }
            return field;
        }

    }
}
